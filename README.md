# Nano DB Manager

## Description

A new back-end in Spring Boot for the Sedaro Nano Interview Assessment Project -NJ
## Table of Contents

1. [Installation](#installation)
2. [Usage](#usage)
3. [Configuration](#configuration)
4. [Endpoints](#endpoints)
5. [Dependencies](#dependencies)
6. [Contributing](#contributing)
7. [License](#license)

## Installation

```bash
git clone https://github.com/your-username/your-project.git
cd nano
mvn install
```
Usage

```mvn spring-boot:run```

Configuration properties

~ application.properties

```server.port=8080```

## Endpoints

    GET /api/data: Retrieve simulation data.

## Dependencies
    Spring Boot (version)
    Spring Data JPA (version)
    H2 Database (version)
    Jakarta Persistence
    Lombok