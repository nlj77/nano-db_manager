
import React, { useEffect, useState } from 'react';
import Plot from 'react-plotly.js';

const App = () => {
  const [plotData, setPlotData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch('http://localhost:8080/api/data');
        if (!response.ok) {
          throw new Error('Failed to fetch data from backend');
        }
        const data = await response.json();

        // Process the received data and extract x and y coordinates
        const processedData = data.map(entry => {
          const { Planet, Satellite } = JSON.parse(entry.data);
          return { Planet: { x: Planet ? Planet.x : null, y: Planet ? Planet.y : null }, Satellite: { x: Satellite ? Satellite.x : null, y: Satellite ? Satellite.y : null } };
        });

        setPlotData(processedData);
        setIsLoading(false);
      } catch (err) {
        console.error('Error fetching data from backend:', err);
        setError(err);
        setIsLoading(false);
      }
    };

    fetchData();
  }, []);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <div className="plot-container">
      <Plot
        data={[
          { x: plotData.map(entry => entry.Planet.x), y: plotData.map(entry => entry.Planet.y), type: 'scatter', mode: 'markers', name: 'Planet' },
          { x: plotData.map(entry => entry.Satellite.x), y: plotData.map(entry => entry.Satellite.y), type: 'scatter', mode: 'markers', name: 'Satellite' }
        ]}
        layout={{
          title: 'Visualization',
          yaxis: { scaleanchor: 'x' },
          width: '100%',
          height: '100%',
        }}
      />
    </div>
  );
};

export default App;

    fetchData();
  }, []);

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <div className="plot-container">
      <Plot
        data={plotData}
        layout={{
          title: 'Visualization',
          yaxis: { scaleanchor: 'x' },
          width: '100%',
          height: '100%',
        }}
      />
    </div>
  );
};
export default App;