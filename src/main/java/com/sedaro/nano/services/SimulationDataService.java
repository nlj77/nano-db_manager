package com.sedaro.nano.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.List;

import com.sedaro.nano.repositories.SimulationDataRepository;
import com.sedaro.nano.entities.SimulationData;


@Service
public class SimulationDataService {
    private final SimulationDataRepository repository;

    public SimulationDataService(SimulationDataRepository repository) {
        this.repository = repository;
    }

    public void importDataFromJson(String jsonFilePath) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            File file = new File(jsonFilePath);
            SimulationData[] data = objectMapper.readValue(file, SimulationData[].class);
            for (SimulationData entry : data) {
                repository.save(entry);
            }
            System.out.println("Data imported successfully.");
        } catch (IOException e) {
            System.err.println("Error reading JSON file: " + e.getMessage());
        }
    }

    public List<SimulationData> getAllData() {
        return repository.findAll();
    }
}
