package com.sedaro.nano.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sedaro.nano.entities.SimulationData;
import org.springframework.stereotype.Repository;

@Repository
public interface SimulationDataRepository extends JpaRepository<SimulationData, Long> {
}