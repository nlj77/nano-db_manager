package com.sedaro.nano.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.sedaro.nano.services.SimulationDataService;

import com.sedaro.nano.entities.SimulationData;

@RestController
public class SimulationDataController {

    @Autowired
    private final SimulationDataService service;

    public SimulationDataController(SimulationDataService service) {
        this.service = service;
    }


    @GetMapping("/api/data")
    public ResponseEntity<List<SimulationData>> getData() {
        try {
            List<SimulationData> data = service.getAllData();
            if (data.isEmpty()) {
                return ResponseEntity.noContent().build(); // Returns 204
            }
            return ResponseEntity.ok(data);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(null);
        }
    }
}