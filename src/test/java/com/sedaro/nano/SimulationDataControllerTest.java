package com.sedaro.nano;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import com.sedaro.nano.controllers.SimulationDataController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.sedaro.nano.entities.SimulationData;
import com.sedaro.nano.services.SimulationDataService;

public class SimulationDataControllerTest {

    @Mock
    private SimulationDataService service;

    @InjectMocks
    private SimulationDataController controller;

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void testGetData() throws Exception {
        // Given
        List<SimulationData> testData = new ArrayList<>();
        testData.add(new SimulationData());
        testData.add(new SimulationData());
        when(service.getAllData()).thenReturn(testData);
        mockMvc.perform(get("/api/data")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
