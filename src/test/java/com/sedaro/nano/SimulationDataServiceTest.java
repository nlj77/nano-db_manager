package com.sedaro.nano;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sedaro.nano.services.SimulationDataService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sedaro.nano.entities.SimulationData;
import com.sedaro.nano.repositories.SimulationDataRepository;

class SimulationDataServiceTest {

    @Mock
    private SimulationDataRepository repository;

    @InjectMocks
    private SimulationDataService service;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testImportDataFromJson() throws IOException {
        // Given
        String jsonFilePath = "test.json";
        SimulationData[] testData = {
                new SimulationData(),
                new SimulationData()
        };

        // Mock repository save method
        when(repository.save(testData[0])).thenReturn(testData[0]);
        when(repository.save(testData[1])).thenReturn(testData[1]);

        // Mock ObjectMapper and File
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File(jsonFilePath);
        objectMapper.writeValue(file, testData);

        // When
        service.importDataFromJson(jsonFilePath);

        // Then
        verify(repository, times(1)).save(testData[0]);
        verify(repository, times(1)).save(testData[1]);
    }

    @Test
    void testGetAllData() {
        // Given
        List<SimulationData> testData = new ArrayList<>();
        testData.add(new SimulationData());
        testData.add(new SimulationData());

        // Mock repository findAll method
        when(repository.findAll()).thenReturn(testData);

        // When
        List<SimulationData> result = service.getAllData();

        assert (result.size() == 2);
    }
}
